
package espol.edu.ec.controller;

import application.MaterialText;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;

/**
 * Profile Controller.
 */
public class ProfileController extends AnchorPane implements Initializable {

    @FXML
    protected AnchorPane menuSection,dashboardSection;
    @FXML
    protected SplitPane splitPane;
    @FXML
    protected ImageView image;
    @FXML
    protected ImageView userimage;
    @FXML
    protected VBox vbox;
    @FXML
    protected MaterialText username;
    @FXML
    protected Label success;
    @FXML
    protected MaterialText bienvenida;
    @FXML
    protected ImageView userimage1;
    
    protected Usuario loggedUser;
    
    protected Main application;
    @FXML
    protected AnchorPane pane;

    public ProfileController() {
    }

    public ProfileController(Usuario loggedUser) {
        this.loggedUser = loggedUser;
    }
  
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {        
        
    }
    
    // se envia un fxml y un pane (lo que hace es cargar el fxml como hijo del pane)
    public Initializable setFXML (String fxml, Pane profile) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = getClass().getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(getClass().getResource(fxml));
        try {
            profile.getChildren().clear();
            AnchorPane userProfile = loader.load(in);
            profile.getChildren().add(userProfile);

        } finally {
            in.close();
        } 
        return (Initializable) loader.getController();
    }
    
    public AnchorPane getMenuSection() {
        return menuSection;
    }

    public void setMenuSection(AnchorPane menuSection) {
        this.menuSection = menuSection;
    }

    public AnchorPane getDashboardSection() {
        return dashboardSection;
    }

    public void setDashboardSection(AnchorPane dashboardSection) {
        this.dashboardSection = dashboardSection;
    }

    public void processLogout() {

        if (application == null){
            return;
        }
        
        application.userLogout();
    }
    
    public void setApp(Main application){
        this.application = application;
        username.setText(loggedUser.getUsuario());  
        byte[] imagen = loggedUser.getImagen();
        if(imagen == null){
            userimage.setImage(new Image("/espol/edu/ec/view/user.png"));
        }
        else{
            BufferedImage img = null;
            try {
                img = ImageIO.read(new ByteArrayInputStream(loggedUser.getImagen()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            userimage.setImage(SwingFXUtils.toFXImage(img, null));
            userimage1.setImage(SwingFXUtils.toFXImage(img, null));
        }
        
        bienvenida.setText(bienvenida.getText()+loggedUser.getNombre());
        menuSection.maxWidthProperty().bind(splitPane.widthProperty().multiply(0.20));
        dashboardSection.maxWidthProperty().bind(splitPane.widthProperty().multiply(0.80));
    }

    public Usuario getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(Usuario loggedUser) {
        this.loggedUser = loggedUser;
        
        

    }
    
   
    
    
}
