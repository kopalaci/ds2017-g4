/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import espol.edu.ec.DBManager.RestaurantJpaController;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Ingrediente;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Usuario;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Ivan
 */
public class RestaurantDetailController implements Initializable {

    @FXML
    private HBox container;
    @FXML
    private TextField dueño;
    @FXML
    private TextField direccion;
    @FXML
    private TextField telefono;
    @FXML
    private Label name;
    @FXML
    private Button save;
    @FXML
    private VBox asistentes;
    private Restaurant restaurant;
    
    private String ambito;
    @FXML
    private AnchorPane header;

    @FXML
    private TableView<Platillo> platillos;
    
    private ProfileController controller;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    public Restaurant getRestaurant() {
        return restaurant;
    }
    public void setAmbito(String ambito){
        this.ambito = ambito;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        if("show".equalsIgnoreCase(ambito)){
            save.setOpacity(0);
            save.setCancelButton(true);
            
        }
        else{
            save.setOpacity(1);
            save.setDisable(false);
            
        }
            
        name.setText(restaurant.getNombre());
        dueño.setText(restaurant.getDueño());
        direccion.setText(restaurant.getUbicacion());
        telefono.setText(String.valueOf(restaurant.getTelefono()));
        for(Usuario asistente : restaurant.getUsuarioCollection()){
            asistentes.getChildren().add((new Text(asistente.getNombre()+" "+asistente.getApellido())));
        }
        fillTableView(restaurant);
        
    }
    
    @FXML
    public void saveRestaurant(ActionEvent event){
        RestaurantJpaController restaurantController =  new RestaurantJpaController();
        try{
            restaurantController.create(restaurant);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("OperaciÃ³n exitosa");
            alert.setHeaderText("Registro exitoso");
            alert.setContentText("Se ha registrado exitosamente el restaurante");
            alert.showAndWait();
        }catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No se pudo conectar");
            alert.setContentText("No se pudo conectar con el servidor");
            alert.showAndWait();
        }
    }
    
    
    public TableView fillTableView(Restaurant restaurante){
        platillos.setTableMenuButtonVisible(true);
 
        TableColumn<Platillo,String> name = new TableColumn<>("Nombre");
        name.setCellValueFactory(new PropertyValueFactory("nombre"));
        name.setPrefWidth(120); 
        
        TableColumn<Platillo,Categoria> categoria = new TableColumn<>("CategorÃ­a");
        categoria.setCellValueFactory(new PropertyValueFactory("idCategoria"));
        categoria.setPrefWidth(120); 
         
        TableColumn<Platillo,String> servido = new TableColumn<>("Servido");
        servido.setCellValueFactory(new PropertyValueFactory("servido"));
        servido.setPrefWidth(120); 
        
        TableColumn<Platillo,String> descripcion = new TableColumn<>("DescripciÃ³n");
        descripcion.setCellValueFactory(new PropertyValueFactory("descripcion"));
        descripcion.setPrefWidth(140); 
        
        TableColumn<Platillo,List<Ingrediente>> ingredientes = new TableColumn<>("Ingredientes");
        ingredientes.setCellValueFactory(new PropertyValueFactory("ingredienteCollection"));
        ingredientes.setPrefWidth(140); 
        
        
        ingredientes.setCellFactory(new Callback<TableColumn<Platillo, List<Ingrediente>>,TableCell<Platillo, List<Ingrediente>>>(){        
            @Override
            public TableCell<Platillo, List<Ingrediente>> call(TableColumn<Platillo, List<Ingrediente>> param) {
                TableCell<Platillo,List<Ingrediente>> cell = new TableCell<Platillo,List<Ingrediente>>(){
                    @Override
                    protected void updateItem(List<Ingrediente> item, boolean empty) {
                        if(item!=null){   
                            VBox vbox = new VBox();
                            for(Ingrediente ingrediente :item){
                                MaterialText name = new MaterialText(ingrediente.getIngrediente());
                                vbox.getChildren().add(name);
                            }
                            setGraphic(vbox);
                        }
                    }

                };
                return cell;
            }
          
        });        
       
        //ADDING ALL THE COLUMNS TO TABLEVIEW
        platillos.getColumns().addAll(name,categoria,servido,descripcion,ingredientes);  
        System.out.println(restaurante);
        ObservableList<Platillo> platillosList = FXCollections.observableList((List)restaurante.getPlatilloCollection());
        platillos.setItems(platillosList);
        
        return platillos;
    }

    public ProfileController getController() {
        return controller;
    }

    public void setController(ProfileController controller) {
        this.controller = controller;
    }
    
    
    @FXML
    private void backEvent(ActionEvent event) {
        controller.getDashboardSection().getChildren().remove( controller.getDashboardSection().getChildren().size()-1);
    }
    
    
}
