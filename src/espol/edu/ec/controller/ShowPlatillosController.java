/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import application.TabTitle;
import espol.edu.ec.model.Imagen;
import espol.edu.ec.model.Platillo;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class ShowPlatillosController implements Initializable {

    @FXML
    private VBox platillos;
    @FXML
    private MaterialText name;
    private ProfileController profileController;
    private Collection<Platillo> platilloCollection;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public void setPlatilloCollection(Collection<Platillo> platilloCollection) {
        this.platilloCollection = platilloCollection;
        
        for (Platillo platillo  :  platilloCollection) {
            ImageView image= image = new ImageView();
            Collection<Imagen> images = platillo.getImagenCollection();
            if(images.isEmpty()){
                image.setImage(new Image("espol/edu/ec/view/generico.png")); 
            }else{
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new ByteArrayInputStream(((List<Imagen>) images).get(0).getImagen()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                image.setImage(SwingFXUtils.toFXImage(img, null));
            }
            image.setFitHeight(60);
            image.setFitWidth(60);
            MaterialText name = new MaterialText(platillo.getNombre()+"  en  "+platillo.getIdRestaurant().getNombre());
            name.setFill(Paint.valueOf("white"));
            TabTitle account = new TabTitle(name);
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));

            name.setOnMouseClicked(event->{
                //profileController.getDashboardSection().getChildren().clear();
                profileController.getDashboardSection().getChildren().add(showDescription(platillo));
            });


            HBox hbox = new HBox(image,account);
            hbox.setAlignment(Pos.CENTER_LEFT);
            hbox.setSpacing(15);
            platillos.getChildren().add(hbox);
            
        }
        
        
    }
    
    public Pane showDescription(Platillo platillo){
        Pane pane = new Pane();
        try {
            PlatilloViewController viewPlatillo = (PlatilloViewController) profileController.setFXML("/espol/edu/ec/view/PlatilloView.fxml", pane);
            viewPlatillo.setController(profileController);
            viewPlatillo.setLoggedUser(profileController.getLoggedUser());
            viewPlatillo.setPlatillo(platillo);
            
        } catch (Exception ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
    

    
    
    public void setProfileController(ProfileController profileController) {
        this.profileController = profileController;
    }

    
    
    
    public Collection<Platillo> getPlatilloCollection() {
        return platilloCollection;
    }
    
    

    
    public ProfileController getProfileController() {
        return profileController;
    }
    
    
    
}
