
package espol.edu.ec.controller;

import espol.edu.ec.DBManager.RestaurantJpaController;
import espol.edu.ec.DBManager.UsuarioJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * Profile Controller.
 */
public class AddUserController extends AddController {

    @FXML
    private TextField user;
   

    @FXML 
    private Button save;
    
    @FXML 
    private Label messagge;
    
    private Main application;
    
    @FXML
    private TextField password;
    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
  
    @FXML
    private ComboBox<String> tipo;
   
    @FXML
    private ComboBox<Restaurant> restaurante;
    
    
    
    ObservableList<String> tipos = FXCollections.observableArrayList(
        "Cliente",
        "Asistente"
    );
   
    @FXML
    private ImageView image;

    
    public void setApp(Main application){
        this.application = application;
        
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        image.setOnMouseClicked((event) -> {
            chooseImage(image);
        });
        messagge.setOpacity(0);
        restaurante.setOpacity(0);
        tipo.getItems().addAll(tipos);
        tipo.setPromptText("Elija una opción");
        tipo.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
                if(ov.getValue()!=null && "Asistente".equalsIgnoreCase(ov.getValue().toString())){
                    restaurante.setOpacity(1);
                }else{
                    restaurante.setOpacity(0);
                }
            }    
        });
        restaurante.getItems().addAll(getRestaurants());
        
    }
    

    
    @FXML
    public void saveProfile(ActionEvent event) throws Exception {
        if (application == null){
            animateMessage();
            return;
        }
        UsuarioJpaController usuarioController = new UsuarioJpaController();
        
        if(user.getText().isEmpty() || password.getText().isEmpty() || nombres.getText().isEmpty()
                || tipo.getSelectionModel().isEmpty() ){
            messagge.setText("Debe llenar todos los campos");
            animateMessage();
            return;
        }
        if(("asistente").equalsIgnoreCase(tipo.getSelectionModel().getSelectedItem()) && restaurante.getSelectionModel().isEmpty()){
            messagge.setText("Debe elegir un restaurante");
            animateMessage();
            return;
        }
        if(usuarioController.findUsuario(user.getText()) != null){
            messagge.setText("El nombre de usuario ya está en uso!");
            animateMessage();
            return;
        }
        else{
            String selectedTipo =  tipo.getSelectionModel().getSelectedItem(); 
            Restaurant selectedRestaurant = restaurante.getSelectionModel().getSelectedItem();
            byte[] imagen = getImageByte();
            Usuario usuario = new Usuario(user.getText(), password.getText(), nombres.getText(), apellidos.getText(),selectedTipo,imagen,selectedRestaurant);
            usuarioController.create(usuario);
            messagge.setText("Usuario agregado exitosamente");
            clearFields();
            animateMessage();
        }
            
            
    }
    
    
    public void clearFields(){
        user.clear();
        password.clear();
        nombres.clear();
        apellidos.clear();
        tipo.valueProperty().set(null);
        restaurante.valueProperty().set(null);
        
    }

    private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), messagge);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    
    public ObservableList<Restaurant> getRestaurants(){
        RestaurantJpaController restauranteController = new RestaurantJpaController();
        ObservableList<Restaurant> restaurantes = FXCollections.observableArrayList((List)restauranteController.findRestaurantEntities()) ;
        return restaurantes;
    }
    
    private void chooseImage(ImageView imageView) {
        
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        File file = fileChooser.showOpenDialog(application.getStage());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setUserData(file);
        } catch (Exception ex) {
            return;
        }
        
    }
    
    public byte[] getImageByte() {
        if(image.getUserData() != null){
            try {
                BufferedImage bImage = SwingFXUtils.fromFXImage(image.getImage(),null);
                ByteArrayOutputStream s = new ByteArrayOutputStream();
                ImageIO.write(bImage, "png", s);
                byte[] imagen = s.toByteArray();
                s.close();
                return imagen;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
