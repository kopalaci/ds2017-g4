/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.DBManager.ImagenJpaController;
import espol.edu.ec.DBManager.IngredienteJpaController;
import espol.edu.ec.DBManager.PlatilloJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Imagen;
import espol.edu.ec.model.Ingrediente;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class AddPlatilloController extends AddController {


    @FXML
    private TextField nombre;
    @FXML
    private TextField categoria;
    @FXML
    private TextField restaurante;
    @FXML
    private TextField descripcion;
    @FXML
    private ComboBox<String> tipo;
    @FXML
    private ComboBox<String> servido;
    @FXML
    private ImageView image1;
    @FXML
    private ImageView image2;
    @FXML
    private ImageView image3;
    @FXML
    private Label messagge;
    @FXML
    private Button save;
    private Main application;
    private Usuario loggedUser;
    private List<ImageView> images ;
    private ObservableList<String> tipos = FXCollections.observableArrayList("Aperitivo","Plato Fuerte", "Postre" );
    private ObservableList<String> servidos = FXCollections.observableArrayList("Frío","Caliente" );

    @FXML
    private ListView<Ingrediente> ingredientes;
    @FXML
    private TextField addIngrediente;
    @FXML
    private Button add;
    private CategoriaJpaController categoriaController;
    private IngredienteJpaController ingredienteController;

    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        images = new LinkedList<>();
        images.add(image1);images.add(image2);images.add(image3);
        tipo.getItems().addAll(tipos);
        servido.getItems().addAll(servidos);
        image1.setOnMouseClicked((event) -> {
            chooseImage(image1);
        });
        image2.setOnMouseClicked((event) -> {
            chooseImage(image2);
        });
        image3.setOnMouseClicked((event) -> {
            chooseImage(image3);
        });
        
        
        
    }    


    @FXML
    private void savePlatillo(ActionEvent event) {
        for(Imagen imgs : getImageCollection()){
            System.out.println(imgs);
        }
        if(nombre.getText().isEmpty() || categoria.getText().isEmpty() || descripcion.getText().isEmpty() 
                ||  tipo.getSelectionModel().isEmpty()  ||  servido.getSelectionModel().isEmpty()){
            messagge.setText("Debe llenar todos los campos");
            animateMessage();
            return;
        }else{
            try {
                Categoria cat = verifyCategoria(categoria.getText());
                if(cat == null){
                    cat = new Categoria(categoria.getText());
                    categoriaController.create(cat);
                }
                Platillo platillo = new Platillo(nombre.getText()
                                                , descripcion.getText()
                                                , tipo.getSelectionModel().getSelectedItem()
                                                , servido.getSelectionModel().getSelectedItem()
                                                ,ingredientes.getItems()
                                                ,getImageCollection()
                                                ,cat
                                                ,loggedUser.getIdRestaurant());
                PlatilloJpaController platilloController = new PlatilloJpaController();
                System.out.println(platillo);
                platilloController.create(platillo);
                messagge.setText("Platillo agregado exitosamente");
                animateMessage();
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(AddPlatilloController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    public void setUser(Usuario usuario){
        this.loggedUser= usuario;
        
        
    }
    
    public Collection<Imagen> getImageCollection() {
        Collection<Imagen> imagenes = new LinkedList<>();
        for (int i = 1; i < images.size()+1; i++) {
            ImageView imgView = images.get(i-1);
            if(imgView.getUserData() != null){
                try {
                    BufferedImage bImage = SwingFXUtils.fromFXImage(imgView.getImage(),null);
                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", s);
                    byte[] res  = s.toByteArray();
                    s.close();
                    ImagenJpaController controller = new ImagenJpaController();
                    controller.create(new Imagen(1,res));
                    List<Imagen> images = controller.findImagenEntities();
                    imagenes.add(images.get(images.size()-1));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                
            }
        }
       
        return imagenes;
    }
    
    
    public void setApp(Main application){
        this.application = application;
        this.loggedUser = application.getLoggedUser();
        restaurante.setText(loggedUser.getIdRestaurant().getNombre());
    }
    
    private void chooseImage(ImageView imageView) {
        
        FileChooser fileChooser = new FileChooser();
        // Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        // Show save file dialog
        File file = fileChooser.showOpenDialog(application.getStage());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setUserData(file);
        } catch (Exception ex) {
            return;
        }
        
    }
    
     private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), messagge);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
     
        
   @FXML
    private void addIngrediente(ActionEvent event) {
        if(!addIngrediente.getText().isEmpty()){
            Ingrediente ingrediente = verifyIngrediente(addIngrediente.getText());
            if(ingrediente == null){
                ingrediente = new Ingrediente();
                ingrediente.setIngrediente(addIngrediente.getText());
                ingredienteController.create(ingrediente);
                
            }
            ingredientes.getItems().add(ingrediente);
        }
        addIngrediente.clear();
        
    }
    
    public Categoria verifyCategoria(String categoriaName){
        categoriaController = new CategoriaJpaController();
        for(Categoria categoria : categoriaController.findCategoriaEntities()){
            if(categoriaName.equalsIgnoreCase(categoria.getCategoria()))
                return categoria;
        }
        return null;
    }
    public Ingrediente verifyIngrediente(String ingredienteName){
        ingredienteController = new IngredienteJpaController();
        for(Ingrediente ingrediente : ingredienteController.findIngredienteEntities()){
            if(ingredienteName.equalsIgnoreCase(ingrediente.getIngrediente()))
                return ingrediente;
        }
        return null;
    }
    
}
