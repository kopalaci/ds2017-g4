/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import application.TabTitle;
import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Platillo;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;

/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class ShowCategoriasController implements Initializable {

    @FXML
    private VBox categorias;
    @FXML
    private MaterialText name;
    
    private Collection<Categoria> CategoriaColletion;
    private ProfileController profileController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

   

   

    public Collection<Categoria> getCategoriaColletion() {
        return CategoriaColletion;
    }

    public void setCategoriaColletion(Collection<Categoria> CategoriaColletion) {
        this.CategoriaColletion = CategoriaColletion;
        for (Categoria c: CategoriaColletion){
            MaterialText text = new MaterialText(c.getCategoria()+ " : "+ c.getPlatilloCollection().size() + " paltillos");
            text.setFill(Paint.valueOf("white"));
            TabTitle btnCategoria = new TabTitle(text);
            btnCategoria.setOnMouseClicked(e->{
                profileController.getDashboardSection().getChildren().clear();
                profileController.getDashboardSection().getChildren().add(mostrarPlatillos(c.getPlatilloCollection()));
            });
            categorias.getChildren().add(btnCategoria);
        }
    }
    
    public Pane mostrarPlatillos(Collection<Platillo> platillos){
        Pane pane = new Pane();
        CategoriaJpaController categoriaController = new CategoriaJpaController();
        List<Categoria> categoriaObjects =categoriaController.findCategoriaEntities();
        try {
            ShowPlatillosController showPlatillosController = (ShowPlatillosController) profileController.setFXML("/espol/edu/ec/view/showPlatillos.fxml", pane);
            showPlatillosController.setProfileController(profileController);
            showPlatillosController.setPlatilloCollection(platillos);
        } catch (Exception ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }

    public ProfileController getProfileController() {
        return profileController;
    }

    public void setProfileController(ProfileController profileController) {
        this.profileController = profileController;
    }
    
    
     
     
    
}
