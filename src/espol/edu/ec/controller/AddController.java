/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.DBManager.ImagenJpaController;
import espol.edu.ec.DBManager.IngredienteJpaController;
import espol.edu.ec.DBManager.PlatilloJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Imagen;
import espol.edu.ec.model.Ingrediente;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class AddController implements Initializable {


    @FXML 
    private Label messagge;
    
    public Main application;
    public Usuario loggedUser;
    public CategoriaJpaController categoriaController;
    public IngredienteJpaController ingredienteController;

    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        
        
    }    

    
    public void setUser(Usuario usuario){
        this.loggedUser= usuario;
        
        
    }
    
       
    
    public void setApp(Main application){
        this.application = application;
        this.loggedUser = application.getLoggedUser();
    }
    
    private void chooseImage(ImageView imageView) {
        
        FileChooser fileChooser = new FileChooser();
        // Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        // Show save file dialog
        File file = fileChooser.showOpenDialog(application.getStage());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setUserData(file);
        } catch (Exception ex) {
            return;
        }
        
    }
    
     private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), messagge);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
     
  
    
}
