/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.DBManager.ImagenJpaController;
import espol.edu.ec.DBManager.IngredienteJpaController;
import espol.edu.ec.DBManager.NonexistentEntityException;
import espol.edu.ec.DBManager.PlatilloJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Imagen;
import espol.edu.ec.model.Ingrediente;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Dario Ntn Carpio
 */
public class PlatilloViewController implements Initializable {

    @FXML
    private AnchorPane globalPane;
    @FXML
    private ListView<String> ingredientes;
    @FXML
    private application.Button addIngrediente;
    @FXML
    private application.Button back;
    @FXML
    private application.Button save;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private TextField categoria;
    @FXML
    private TextField servido;
    @FXML
    private TextField descripcion;
    @FXML
    private TextField restaurante;
    @FXML
    private TextField name;
    
    private Usuario loggedUser;
    private Platillo platillo;
    private ProfileController controller;
    @FXML
    private Label messagge;

    private LinkedList<ImageView> images;
    
    
  
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        images = new LinkedList<>();
        images.add(img1);images.add(img2);images.add(img3);
        
        for (ImageView iv: images){
            iv.setOnMouseEntered(new zoominEvent());
            iv.setOnMouseExited(new zoomoutEvent());
        }
        
    }

    @FXML
    private void backEvent(ActionEvent event) {
        controller.getDashboardSection().getChildren().remove( controller.getDashboardSection().getChildren().size()-1);
    }

    @FXML
    private void saveEvent(ActionEvent event) {
        if(name.getText().isEmpty()|| categoria.getText().isEmpty()||servido.getText().isEmpty()
                ||descripcion.getText().isEmpty()|| ingredientes.getItems().isEmpty()){
            messagge.setText("Debe llenar todos los campos");
            animateMessage();
            return;
        }
        platillo.setNombre(name.getText());
        Categoria cat = verifyCategoria(categoria.getText());
        if(cat == null){
            cat = new Categoria(categoria.getText());
        }
        if("frio".equalsIgnoreCase(servido.getText()) ||"frío".equalsIgnoreCase(servido.getText())|| "caliente".equalsIgnoreCase(servido.getText())){
            platillo.setServido((servido.getText()));
        }
        else{
            messagge.setText("Servido solo puede ser Frio o Caliente");
            animateMessage();
            return;
        }
        
        platillo.setDescripcion(descripcion.getText());
        platillo.getIngredienteCollection().clear();
        for(String ingr : ingredientes.getItems()){
            Ingrediente verified = verifyIngrediente(ingr);
            if(verified == null){
                IngredienteJpaController controller = new IngredienteJpaController();
                List<Ingrediente> lista = controller.findIngredienteEntities();
                controller.create(new Ingrediente(1, ingr));
                platillo.getIngredienteCollection().add(controller.findIngrediente(lista.size()+10));
            }else
                platillo.getIngredienteCollection().add(verified);
        }
        setImageCollection(platillo.getImagenCollection());
        PlatilloJpaController platilloController = new PlatilloJpaController();
        try {
            platilloController.edit(platillo);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PlatilloViewController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PlatilloViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        messagge.setText("Platillo modificado Exitosamente");
        animateMessage();
                
    }
    
    
     private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), messagge);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    @FXML
    private void addEvent(ActionEvent event) {
        ingredientes.getItems().add("");
        ingredientes.scrollTo(ingredientes.getItems().size() - 1);
        ingredientes.layout();
        ingredientes.edit(ingredientes.getItems().size() - 1);
    }
    
    private class zoominEvent<MouseEvent> implements EventHandler{
        ImageView img;
        @Override
        public void handle(Event event) {
            img = (ImageView)event.getSource();

            img.setScaleX(1.6);
            img.setScaleY(1.6);            
        }
    }
    
    private class zoomoutEvent<MouseEvent> implements EventHandler{
        ImageView img;
        @Override
        public void handle(Event event) {
            img = (ImageView)event.getSource();

            img.setScaleX(1);
            img.setScaleY(1);
        }
    }
    
    public void setImageCollection(Collection<Imagen> imagenes) {
        imagenes.clear();
        ImagenJpaController controller = new ImagenJpaController();
        for (int i = 1; i < images.size()+1; i++) {
            ImageView imageView = images.get(i-1);
            
            try {
                
                Imagen userData= ((Imagen)imageView.getUserData());
                BufferedImage bImage = SwingFXUtils.fromFXImage(imageView.getImage(),null);
                ByteArrayOutputStream s = new ByteArrayOutputStream();
                ImageIO.write(bImage, "png", s);
                byte[] res  = s.toByteArray();
                s.close();
                imagenes.add(controller.findImagen(userData.getIdImagen()));
                
            }catch(Exception e){
                try{
                    File file =  ((File)imageView.getUserData()); 
                    BufferedImage bImage = SwingFXUtils.fromFXImage(imageView.getImage(),null);
                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", s);
                    byte[] res  = s.toByteArray();
                    s.close();
                    try{
                        controller.create(new Imagen(1, res));
                    }catch(Exception ex2){
                        messagge.setText("imagen muy grande");
                        animateMessage();
                    }
                   
                    List<Imagen> images = controller.findImagenEntities();
                    Imagen img = images.get(images.size()-1); 
                    System.out.println(img.getIdImagen());
                    imagenes.add(img);
                }catch(Exception ex){
                    System.out.println("no es ni file ni integer");
                }
                
             }
            
            
            
        }
     }

    public void setPlatillo(Platillo platillo) {
        this.platillo = platillo;
        name.setText(platillo.getNombre());
        restaurante.setText(platillo.getIdRestaurant().getNombre());
        categoria.setText(platillo.getIdCategoria().getCategoria());
        servido.setText(platillo.getServido());
        descripcion.setText(platillo.getDescripcion());
        List<String> list = new ArrayList<>();
        for(Ingrediente ingrediente : platillo.getIngredienteCollection()){
            list.add(ingrediente.getIngrediente());
        }
        ObservableList<String> ingredientesList = FXCollections.observableList(list);
        ingredientes.setItems(ingredientesList);
        for (int i = 0; i < platillo.getImagenCollection().size(); i++) {
            Imagen image = ((List<Imagen>)platillo.getImagenCollection()).get(i);
            if(image != null){
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new ByteArrayInputStream(image.getImagen()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                images.get(i).setImage(SwingFXUtils.toFXImage(img, null));
                images.get(i).setUserData(image);
            }
          
        }

        
         
        
    }

    public void setLoggedUser(Usuario loggedUser) {
        this.loggedUser = loggedUser;
        if("asistente".equalsIgnoreCase(loggedUser.getTipo())){
            img1.setOnMouseClicked((event) -> {
            chooseImage(img1);
            });
            img2.setOnMouseClicked((event) -> {
                chooseImage(img2);
            });
            img3.setOnMouseClicked((event) -> {
                chooseImage(img3);
            });
            categoria.setEditable(true);
            servido.setEditable(true);
            descripcion.setEditable(true);
            name.setEditable(true);
            ingredientes.setEditable(true);
            save.setDisable(false);
            save.setOpacity(1);
            addIngrediente.setDisable(false);
            addIngrediente.setOpacity(1);
            ingredientes.setCellFactory(TextFieldListCell.forListView());
            ingredientes.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
                                if("".equalsIgnoreCase(t.getNewValue()))
                                    ingredientes.getItems().remove(t.getIndex());
                                else
                                    ingredientes.getItems().set(t.getIndex(), t.getNewValue());
				
			}
						
		});

            ingredientes.setOnEditCancel(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
				
			}
		});   
                    
        }
    }
    
    public Categoria verifyCategoria(String categoriaName){
        CategoriaJpaController categoriaController = new CategoriaJpaController();
        for(Categoria categoria : categoriaController.findCategoriaEntities()){
            if(categoriaName.equalsIgnoreCase(categoria.getCategoria()))
                return categoria;
        }
        return null;
    }
    public Ingrediente verifyIngrediente(String ingredienteName){
        IngredienteJpaController ingredienteController = new IngredienteJpaController();
        for(Ingrediente ingrediente : ingredienteController.findIngredienteEntities()){
            if(ingredienteName.equalsIgnoreCase(ingrediente.getIngrediente()))
                return ingrediente;
        }
        return null;
    }

    private void chooseImage(ImageView imageView) {
        
        FileChooser fileChooser = new FileChooser();
        // Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        // Show save file dialog
        File file = fileChooser.showOpenDialog(controller.application.getStage());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setUserData(file);
         
            
        } catch (Exception ex) {
            return;
        }
        
    }

    public ProfileController getController() {
        return controller;
    }

    public void setController(ProfileController controller) {
        this.controller = controller;
    }
    
    public Usuario getLoggedUser() {
        return loggedUser;
    }
    public Platillo getPlatillo() {
        return platillo;
    }
    
    

  
}