
package espol.edu.ec.controller;

import espol.edu.ec.main.Main;
import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * Login Controller.
 */
public class LoginController extends AnchorPane implements Initializable {

    @FXML
    TextField userId;
    @FXML
    PasswordField password;
    @FXML
    Button login;
    @FXML
    Label errorMessage;

    private Main application;
    
    
    public void setApp(Main application){
        this.application = application;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessage.setText("");
        userId.setPromptText("Usuario");
        password.setPromptText("Contraseña");
        
    }
    
    //al presionar boton login 
    public void processLogin(ActionEvent event) {
        if (application == null){

            errorMessage.setText("Hola " + userId.getText());
        } else {
            int session = application.userLogging(userId.getText(), password.getText());
            if (session ==0){
                errorMessage.setText("Username/Password is incorrect");
            }
            if(session == -1)
                errorMessage.setText("Falló la conexion con el servidor");
       }
    }
}
