/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import application.TabTitle;
import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.DBManager.PlatilloJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class UserProfileController extends ProfileController {
  
    public UserProfileController(Usuario loggedUser) {
        super(loggedUser);
    }

    
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        {
            TabTitle account = new TabTitle(new MaterialText("Listar Categorias"));
            account.setPrefSize(240, 50);
            account.setPadding(new Insets(20));
            account.setTextFill(Color.WHITE);
            account.setAlignment(Pos.CENTER_LEFT);
            account.setOnMouseClicked((MouseEvent e) -> {
                dashboardSection.getChildren().add(listarCategorias());
                
            });
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Buscar Platillo"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                ScrollPane platillos = buscarPlatillos();
                dashboardSection.getChildren().add(platillos);
                
            });
            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Cerrar Sesión"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                processLogout();
                
            });

            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }

        
    }  
    public Pane listarCategorias(){
        Pane pane = new Pane();
        CategoriaJpaController categoriaController = new CategoriaJpaController();
        List<Categoria> categoriaObjects =categoriaController.findCategoriaEntities();
        try {
            ShowCategoriasController showCategory = (ShowCategoriasController) setFXML("/espol/edu/ec/view/showCategorias.fxml", pane);
            showCategory.setProfileController(this);
            showCategory.setCategoriaColletion(categoriaObjects);
        } catch (Exception ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }

       
//    public ScrollPane listarCategoria(){
//        ScrollPane scroll = new ScrollPane();
//        ListView categorias = new ListView();
//        categorias.setOnMouseClicked(new EventHandler<MouseEvent>() {
//
//            @Override
//            public void handle(MouseEvent event) {
//                dashboardSection.getChildren().clear();
//                Categoria categoria = (Categoria)categorias.getSelectionModel().getSelectedItem();
//                ScrollPane platillos =  mostrarPlatillos(categoria.getPlatilloCollection());
//                dashboardSection.getChildren().add(platillos);
//            }
//        });
//        scroll.setFitToHeight(true);
//        scroll.setFitToWidth(true);
//        CategoriaJpaController categoriaController = new CategoriaJpaController();
//        List<Categoria> categoriaObjects =categoriaController.findCategoriaEntities();
//        
//          
//        
//        ObservableList<Categoria> observableList = FXCollections.observableList(categoriaObjects);
//        categorias.setItems(observableList);
//        scroll.setContent(categorias);
//        scroll.prefWidthProperty().bind(dashboardSection.widthProperty());
//        scroll.prefHeightProperty().bind(dashboardSection.heightProperty());
//        return scroll;
//    }
    
    public Pane showDescription(Platillo platillo){
        Pane pane = new Pane();
        try {
            PlatilloViewController viewPlatillo = (PlatilloViewController) setFXML("/espol/edu/ec/view/PlatilloView.fxml", pane);
            viewPlatillo.setController(this);
            viewPlatillo.setPlatillo(platillo);
        } catch (Exception ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
    
    private ScrollPane buscarPlatillos() {
        PlatilloJpaController platilloController = new PlatilloJpaController();
        ScrollPane busqueda = new ScrollPane();
        VBox vbox = new VBox();
        Label label = new Label("Buscar ");
        //ObservableList<PlatilloInfo> platillos = getPlatillosInfo();
        ObservableList<Platillo> platillos = FXCollections.observableArrayList(platilloController.findPlatilloEntities());
        TableView tableView = crearTabla();
        //JFXTreeTableView<PlatilloInfo> treeView = crearTabla(getPlatillosInfo());        
        TextField text = new TextField();
        text.setPromptText("Busqueda de platillos");        
        /*
        treeView.setVisible(false);
        text.textProperty().addListener((o, oldVal, newVal) -> {
            if (newVal.length()>0){
                treeView.setVisible(true);
            }else{
                treeView.setVisible(false);
            }
            treeView.setPredicate(userProp -> {
                final PlatilloInfo platillo = userProp.getValue();
                return platillo.getNombre().toLowerCase().contains(newVal.toLowerCase())
                    || platillo.getDescripcion().toLowerCase().contains(newVal.toLowerCase());
            });
        });
        */
        text.textProperty().addListener((o, oldVal, newVal) -> {
            if (newVal.length()>0){
                tableView.setVisible(true);
            }else{
                tableView.setVisible(false);
            }
            
            ObservableList filteredList = platillos.filtered(predicate->{
                return predicate.getNombre().toLowerCase().contains(newVal.toLowerCase())
                        || predicate.getDescripcion().toLowerCase().contains(newVal.toLowerCase());
            });
            tableView.setItems(filteredList);
        });
        
        
        vbox.getChildren().addAll(label,text,tableView);
        busqueda.setContent(vbox);
        
        busqueda.prefWidthProperty().bind(dashboardSection.widthProperty());
        busqueda.prefHeightProperty().bind(dashboardSection.heightProperty());
        
        return busqueda;
    }
    
    private TableView crearTabla(){
        TableView<Platillo> table = new TableView<>(); 
 
        TableColumn<Platillo,String> name = new TableColumn<>("Nombre");
        name.setCellValueFactory(new PropertyValueFactory("nombre"));
        name.setPrefWidth(320);
        
        TableColumn<Platillo,String> restaurant = new TableColumn<>("Restaurante");
        restaurant.setCellValueFactory(new PropertyValueFactory("idRestaurant"));
        restaurant.setPrefWidth(320);
        
        table.setOnMouseClicked(e->{
            Pane descripcion = showDescription(table.getSelectionModel().getSelectedItem());
            dashboardSection.getChildren().add(descripcion);
        });
        
        table.prefWidthProperty().bind(dashboardSection.widthProperty());
        table.prefHeightProperty().bind(dashboardSection.heightProperty());
        table.getColumns().addAll(name,restaurant);
        
        table.setItems(FXCollections.observableArrayList());
        
        return table;
    }
    
//    public ScrollPane mostrarPlatillos(Collection<Platillo> platillos){
//        ScrollPane scroll = new ScrollPane();
//        VBox vbox = new VBox();
//        vbox.setSpacing(5);
//        
//        scroll.setFitToHeight(true);
//        scroll.setFitToWidth(true);
//        
//        
//        for (Platillo platillo  :  platillos) {
//            ImageView image = new ImageView("espol/edu/ec/view/cat.png");
//                image.setFitHeight(60);
//                image.setFitWidth(60);
//                MaterialText name = new MaterialText(platillo.getNombre());
//                TabTitle account = new TabTitle(name);
//                account.setPrefSize(240, 50);
//                account.setTextFill(Color.GREY);
//                account.setPadding(new Insets(20));
//
//
//                HBox hbox = new HBox(image,account);
//                hbox.setAlignment(Pos.CENTER_LEFT);
//                hbox.setSpacing(15);
//                vbox.getChildren().add(hbox);
//            
//        }
//        
//
//        scroll.setContent(vbox);
//        scroll.prefWidthProperty().bind(dashboardSection.widthProperty());
//        scroll.prefHeightProperty().bind(dashboardSection.heightProperty());
//        return scroll;
//    }
//    
//    public Collection<Platillo> buscarPlatillo(String busqueda){
//        PlatilloJpaController platilloController = new PlatilloJpaController();
//        Collection<Platillo> resultados = new LinkedList<>();
//        for(Platillo platillo :  platilloController.findPlatilloEntities()){
//            if(platillo.getDescripcion().toLowerCase().contains(busqueda.toLowerCase()))
//                resultados.add(platillo);
//        }
//        return resultados;
//    }
}
