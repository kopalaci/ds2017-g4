/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import application.TabTitle;
import espol.edu.ec.DBManager.CategoriaJpaController;
import espol.edu.ec.DBManager.UsuarioJpaController;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Usuario;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author 
 */
public class AssistantProfileController extends ProfileController {

     
    private List<Restaurant> restaurantObjects;
    private Restaurant restaurante;

    public AssistantProfileController(Usuario loggedUser) {
        super(loggedUser);
    }

    
    
    
    public Restaurant getRestaurante() {
        return restaurante;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        {
            TabTitle account = new TabTitle(new MaterialText("Agregar Platillo"));
            account.setPrefSize(150, 50);
            account.setPadding(new Insets(20));
            account.setTextFill(Color.WHITE);
            account.setAlignment(Pos.CENTER_LEFT);
            account.setOnMouseClicked((MouseEvent e) -> {
                Pane newPlatillo = agregarPlatillo();
                newPlatillo.setPrefHeight(500);
                dashboardSection.getChildren().clear();
                dashboardSection.getChildren().add(newPlatillo);
            });
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Listar Platillos"));
            account.setPrefSize(150, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                dashboardSection.getChildren().clear();
                UsuarioJpaController controller = new UsuarioJpaController();
                loggedUser= controller.findUsuario(loggedUser.getUsuario());    
                dashboardSection.getChildren().add(listarPlatillos(loggedUser.getIdRestaurant().getPlatilloCollection()));
                                
            });
            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Listar Categorias"));
            account.setPrefSize(150, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                dashboardSection.getChildren().clear();
                dashboardSection.getChildren().add(listarCategorias());

                
            });
            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        
        {
            TabTitle account = new TabTitle(new MaterialText("Cerrar Sesión"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                processLogout();
                
            });

            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        
            
    }  
      
    public Pane agregarPlatillo(){
        Pane pane = new Pane();
        try {
            AddPlatilloController newPlatillo = (AddPlatilloController) setFXML("/espol/edu/ec/view/addPlatillo.fxml", pane);
            newPlatillo.setApp(application);
        } catch (Exception ex) {
            Logger.getLogger(AssistantProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
    
    public Pane listarPlatillos(Collection<Platillo> platillos){
        Pane pane = new Pane();
        try {
            ShowPlatillosController showPlatillosController = (ShowPlatillosController) setFXML("/espol/edu/ec/view/showPlatillos.fxml", pane);
            showPlatillosController.setProfileController(this);
            showPlatillosController.setPlatilloCollection(platillos);
        } catch (Exception ex) {
            Logger.getLogger(AssistantProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
    
    public Pane listarCategorias(){
        Pane pane = new Pane();
        List<Categoria> categoriaMiRerstaurante =new LinkedList<>();
        CategoriaJpaController categoriaController = new CategoriaJpaController();
        List<Categoria> categoriaObjects =categoriaController.findCategoriaEntities();
        for(Categoria categoria : categoriaObjects){
            if(categoria.getRestaurantCollection().contains(loggedUser.getIdRestaurant()))
                categoriaMiRerstaurante.add(categoria);
        }
        try {
            ShowCategoriasController showCategory = (ShowCategoriasController) setFXML("/espol/edu/ec/view/showCategorias.fxml", pane);
            showCategory.setProfileController(this);
            showCategory.setCategoriaColletion(categoriaMiRerstaurante);
        } catch (Exception ex) {
            Logger.getLogger(AssistantProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
    
}
