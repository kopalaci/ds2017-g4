/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import application.MaterialText;
import application.TabTitle;
import espol.edu.ec.DBManager.RestaurantJpaController;
import espol.edu.ec.main.Main;
import espol.edu.ec.model.Ingrediente;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;



/**
 * FXML Controller class
 *
 * @author Palacios
 */
public class AdminProfileController extends ProfileController {

    private List<Restaurant> restaurantObjects;
    private Restaurant restaurante;

    public AdminProfileController(Usuario loggedUser) {
        super(loggedUser);
    }

    

        
    public Restaurant getRestaurante() {
        return restaurante;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        {
            TabTitle account = new TabTitle(new MaterialText("Agregar Restaurante"));
            account.setPrefSize(240, 50);
            account.setPadding(new Insets(20));
            account.setTextFill(Color.WHITE);
            account.setAlignment(Pos.CENTER_LEFT);
            account.setOnMouseClicked((MouseEvent e) -> {
                Restaurant restaurant = chooseRestaurant();
                if(restaurant != null){
                    dashboardSection.getChildren().add(mostrarRestaurante(restaurant , "add"));
                }
            });
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Listar Restaurante"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                ScrollPane scroll = listarRestaurantes();
                dashboardSection.getChildren().add(scroll);
                scroll.prefWidthProperty().bind(dashboardSection.widthProperty());
                scroll.prefHeightProperty().bind(dashboardSection.heightProperty());

                                
            });
            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Agregar Usuario"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                Pane addUser = agregarUsuario();
                addUser.setPrefHeight(500);
                dashboardSection.getChildren().add(addUser);
            });
            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        {
            TabTitle account = new TabTitle(new MaterialText("Cerrar Sesión"));
            account.setPrefSize(240, 50);
            account.setTextFill(Color.GREY);
            account.setPadding(new Insets(20));
            account.setOnMouseClicked((MouseEvent e) -> {
                processLogout();
                
            });

            account.setAlignment(Pos.CENTER_LEFT);
            vbox.getChildren().add(account);
        }
        
    }  
    
    public ScrollPane listarRestaurantes(){
        ScrollPane scroll = new ScrollPane();
        ListView restaurantes = new ListView();
        restaurantes.setOnMouseClicked(new EventHandler<MouseEvent>() {
           

            @Override
            public void handle(MouseEvent event) {
                //dashboardSection.getChildren().clear();
                restaurante = (Restaurant)restaurantes.getSelectionModel().getSelectedItem();
                Pane restauranteDetail =  mostrarRestaurante(restaurante , "show");
                dashboardSection.getChildren().add(restauranteDetail);
            }
        });
        scroll.setFitToHeight(true);
        scroll.setFitToWidth(true);
        RestaurantJpaController restaurantController = new RestaurantJpaController();
        restaurantObjects =restaurantController.findRestaurantEntities();
     
        ObservableList<Restaurant> observableList = FXCollections.observableList(restaurantObjects);
        restaurantes.setItems(observableList);
        scroll.setContent(restaurantes);

        return scroll;
    }
    
    private Restaurant chooseRestaurant() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter( "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        // Show save file dialog
        File file = fileChooser.showOpenDialog(application.getStage());
        Restaurant restaurant = null;
        if (file != null) {
            restaurant = loadRestaurantDataFromFile(file);
        }
        return restaurant;
    }
    
    public Restaurant loadRestaurantDataFromFile(File file){
        try {
            JAXBContext context = JAXBContext.newInstance(Restaurant.class);
            Unmarshaller um = context.createUnmarshaller();

            Restaurant restaurante = (Restaurant) um.unmarshal(file);
            return restaurante;

        } catch (Exception e) { // catches ANY exception
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No se puede cargar");
            alert.setContentText("No se puede cargar la informarción del archivo");
            alert.showAndWait();
        }
        return null;
    }
        
    
    public Pane mostrarRestaurante(Restaurant restaurante , String ambito){
        Pane pane = new Pane();
        try {
            RestaurantDetailController restDetail = (RestaurantDetailController) setFXML("/espol/edu/ec/view/restaurantDetail.fxml", pane);
            restDetail.setController(this);
            restDetail.setAmbito(ambito);
            restDetail.setRestaurant(restaurante);
        } catch (Exception ex) {
            Logger.getLogger(AdminProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        return pane;
    }
    
    public Pane agregarUsuario(){
        Pane pane = new Pane();
        try {
            AddUserController addUser = (AddUserController) setFXML("/espol/edu/ec/view/addUser.fxml", pane);
            addUser.setApp(application);
        } catch (Exception ex) {
            Logger.getLogger(AdminProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }
 
    
   
    public void setRestaurante(Restaurant restaurante) {
        this.restaurante = restaurante;
    }
    
    public List<Restaurant> getRestaurantObjects() {
        return restaurantObjects;
    }

    public void setRestaurantObjects(List<Restaurant> restaurantObjects) {
        this.restaurantObjects = restaurantObjects;
    }
}