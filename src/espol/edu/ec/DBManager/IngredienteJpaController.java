/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.DBManager;

import espol.edu.ec.model.Ingrediente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import espol.edu.ec.model.Platillo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Palacios
 */
public class IngredienteJpaController implements Serializable {

    public IngredienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("CatalogoDeDelicias2PU");
        }
        return emf.createEntityManager();
    }

    public IngredienteJpaController() {
    }
    
    public void create(Ingrediente ingrediente) {
        if (ingrediente.getPlatilloCollection() == null) {
            ingrediente.setPlatilloCollection(new ArrayList<Platillo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Platillo> attachedPlatilloCollection = new ArrayList<Platillo>();
            for (Platillo platilloCollectionPlatilloToAttach : ingrediente.getPlatilloCollection()) {
                platilloCollectionPlatilloToAttach = em.getReference(platilloCollectionPlatilloToAttach.getClass(), platilloCollectionPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollection.add(platilloCollectionPlatilloToAttach);
            }
            ingrediente.setPlatilloCollection(attachedPlatilloCollection);
            em.persist(ingrediente);
            for (Platillo platilloCollectionPlatillo : ingrediente.getPlatilloCollection()) {
                platilloCollectionPlatillo.getIngredienteCollection().add(ingrediente);
                platilloCollectionPlatillo = em.merge(platilloCollectionPlatillo);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingrediente ingrediente) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingrediente persistentIngrediente = em.find(Ingrediente.class, ingrediente.getIdIngrediente());
            Collection<Platillo> platilloCollectionOld = persistentIngrediente.getPlatilloCollection();
            Collection<Platillo> platilloCollectionNew = ingrediente.getPlatilloCollection();
            Collection<Platillo> attachedPlatilloCollectionNew = new ArrayList<Platillo>();
            for (Platillo platilloCollectionNewPlatilloToAttach : platilloCollectionNew) {
                platilloCollectionNewPlatilloToAttach = em.getReference(platilloCollectionNewPlatilloToAttach.getClass(), platilloCollectionNewPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollectionNew.add(platilloCollectionNewPlatilloToAttach);
            }
            platilloCollectionNew = attachedPlatilloCollectionNew;
            ingrediente.setPlatilloCollection(platilloCollectionNew);
            ingrediente = em.merge(ingrediente);
            for (Platillo platilloCollectionOldPlatillo : platilloCollectionOld) {
                if (!platilloCollectionNew.contains(platilloCollectionOldPlatillo)) {
                    platilloCollectionOldPlatillo.getIngredienteCollection().remove(ingrediente);
                    platilloCollectionOldPlatillo = em.merge(platilloCollectionOldPlatillo);
                }
            }
            for (Platillo platilloCollectionNewPlatillo : platilloCollectionNew) {
                if (!platilloCollectionOld.contains(platilloCollectionNewPlatillo)) {
                    platilloCollectionNewPlatillo.getIngredienteCollection().add(ingrediente);
                    platilloCollectionNewPlatillo = em.merge(platilloCollectionNewPlatillo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingrediente.getIdIngrediente();
                if (findIngrediente(id) == null) {
                    throw new NonexistentEntityException("The ingrediente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingrediente ingrediente;
            try {
                ingrediente = em.getReference(Ingrediente.class, id);
                ingrediente.getIdIngrediente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingrediente with id " + id + " no longer exists.", enfe);
            }
            Collection<Platillo> platilloCollection = ingrediente.getPlatilloCollection();
            for (Platillo platilloCollectionPlatillo : platilloCollection) {
                platilloCollectionPlatillo.getIngredienteCollection().remove(ingrediente);
                platilloCollectionPlatillo = em.merge(platilloCollectionPlatillo);
            }
            em.remove(ingrediente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingrediente> findIngredienteEntities() {
        return findIngredienteEntities(true, -1, -1);
    }

    public List<Ingrediente> findIngredienteEntities(int maxResults, int firstResult) {
        return findIngredienteEntities(false, maxResults, firstResult);
    }

    private List<Ingrediente> findIngredienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingrediente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingrediente findIngrediente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingrediente.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngredienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingrediente> rt = cq.from(Ingrediente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
