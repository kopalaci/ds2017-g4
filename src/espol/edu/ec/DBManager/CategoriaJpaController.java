/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.DBManager;

import espol.edu.ec.model.Categoria;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import espol.edu.ec.model.Restaurant;
import java.util.ArrayList;
import java.util.Collection;
import espol.edu.ec.model.Platillo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Palacios
 */
public class CategoriaJpaController implements Serializable {

    public CategoriaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("CatalogoDeDelicias2PU");
        }
        return emf.createEntityManager();
    }

    public CategoriaJpaController() {
    }
    
    

    public void create(Categoria categoria) throws PreexistingEntityException, Exception {
        if (categoria.getRestaurantCollection() == null) {
            categoria.setRestaurantCollection(new ArrayList<Restaurant>());
        }
        if (categoria.getPlatilloCollection() == null) {
            categoria.setPlatilloCollection(new ArrayList<Platillo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Restaurant> attachedRestaurantCollection = new ArrayList<Restaurant>();
            for (Restaurant restaurantCollectionRestaurantToAttach : categoria.getRestaurantCollection()) {
                restaurantCollectionRestaurantToAttach = em.getReference(restaurantCollectionRestaurantToAttach.getClass(), restaurantCollectionRestaurantToAttach.getIdRestaurant());
                attachedRestaurantCollection.add(restaurantCollectionRestaurantToAttach);
            }
            categoria.setRestaurantCollection(attachedRestaurantCollection);
            Collection<Platillo> attachedPlatilloCollection = new ArrayList<Platillo>();
            for (Platillo platilloCollectionPlatilloToAttach : categoria.getPlatilloCollection()) {
                platilloCollectionPlatilloToAttach = em.getReference(platilloCollectionPlatilloToAttach.getClass(), platilloCollectionPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollection.add(platilloCollectionPlatilloToAttach);
            }
            categoria.setPlatilloCollection(attachedPlatilloCollection);
            em.persist(categoria);
            for (Restaurant restaurantCollectionRestaurant : categoria.getRestaurantCollection()) {
                restaurantCollectionRestaurant.getCategoriaCollection().add(categoria);
                restaurantCollectionRestaurant = em.merge(restaurantCollectionRestaurant);
            }
            for (Platillo platilloCollectionPlatillo : categoria.getPlatilloCollection()) {
                Categoria oldIdCategoriaOfPlatilloCollectionPlatillo = platilloCollectionPlatillo.getIdCategoria();
                platilloCollectionPlatillo.setIdCategoria(categoria);
                platilloCollectionPlatillo = em.merge(platilloCollectionPlatillo);
                if (oldIdCategoriaOfPlatilloCollectionPlatillo != null) {
                    oldIdCategoriaOfPlatilloCollectionPlatillo.getPlatilloCollection().remove(platilloCollectionPlatillo);
                    oldIdCategoriaOfPlatilloCollectionPlatillo = em.merge(oldIdCategoriaOfPlatilloCollectionPlatillo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCategoria(categoria.getCategoria()) != null) {
                throw new PreexistingEntityException("Categoria " + categoria + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categoria categoria) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria persistentCategoria = em.find(Categoria.class, categoria.getCategoria());
            Collection<Restaurant> restaurantCollectionOld = persistentCategoria.getRestaurantCollection();
            Collection<Restaurant> restaurantCollectionNew = categoria.getRestaurantCollection();
            Collection<Platillo> platilloCollectionOld = persistentCategoria.getPlatilloCollection();
            Collection<Platillo> platilloCollectionNew = categoria.getPlatilloCollection();
            List<String> illegalOrphanMessages = null;
            for (Platillo platilloCollectionOldPlatillo : platilloCollectionOld) {
                if (!platilloCollectionNew.contains(platilloCollectionOldPlatillo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Platillo " + platilloCollectionOldPlatillo + " since its idCategoria field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Restaurant> attachedRestaurantCollectionNew = new ArrayList<Restaurant>();
            for (Restaurant restaurantCollectionNewRestaurantToAttach : restaurantCollectionNew) {
                restaurantCollectionNewRestaurantToAttach = em.getReference(restaurantCollectionNewRestaurantToAttach.getClass(), restaurantCollectionNewRestaurantToAttach.getIdRestaurant());
                attachedRestaurantCollectionNew.add(restaurantCollectionNewRestaurantToAttach);
            }
            restaurantCollectionNew = attachedRestaurantCollectionNew;
            categoria.setRestaurantCollection(restaurantCollectionNew);
            Collection<Platillo> attachedPlatilloCollectionNew = new ArrayList<Platillo>();
            for (Platillo platilloCollectionNewPlatilloToAttach : platilloCollectionNew) {
                platilloCollectionNewPlatilloToAttach = em.getReference(platilloCollectionNewPlatilloToAttach.getClass(), platilloCollectionNewPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollectionNew.add(platilloCollectionNewPlatilloToAttach);
            }
            platilloCollectionNew = attachedPlatilloCollectionNew;
            categoria.setPlatilloCollection(platilloCollectionNew);
            categoria = em.merge(categoria);
            for (Restaurant restaurantCollectionOldRestaurant : restaurantCollectionOld) {
                if (!restaurantCollectionNew.contains(restaurantCollectionOldRestaurant)) {
                    restaurantCollectionOldRestaurant.getCategoriaCollection().remove(categoria);
                    restaurantCollectionOldRestaurant = em.merge(restaurantCollectionOldRestaurant);
                }
            }
            for (Restaurant restaurantCollectionNewRestaurant : restaurantCollectionNew) {
                if (!restaurantCollectionOld.contains(restaurantCollectionNewRestaurant)) {
                    restaurantCollectionNewRestaurant.getCategoriaCollection().add(categoria);
                    restaurantCollectionNewRestaurant = em.merge(restaurantCollectionNewRestaurant);
                }
            }
            for (Platillo platilloCollectionNewPlatillo : platilloCollectionNew) {
                if (!platilloCollectionOld.contains(platilloCollectionNewPlatillo)) {
                    Categoria oldIdCategoriaOfPlatilloCollectionNewPlatillo = platilloCollectionNewPlatillo.getIdCategoria();
                    platilloCollectionNewPlatillo.setIdCategoria(categoria);
                    platilloCollectionNewPlatillo = em.merge(platilloCollectionNewPlatillo);
                    if (oldIdCategoriaOfPlatilloCollectionNewPlatillo != null && !oldIdCategoriaOfPlatilloCollectionNewPlatillo.equals(categoria)) {
                        oldIdCategoriaOfPlatilloCollectionNewPlatillo.getPlatilloCollection().remove(platilloCollectionNewPlatillo);
                        oldIdCategoriaOfPlatilloCollectionNewPlatillo = em.merge(oldIdCategoriaOfPlatilloCollectionNewPlatillo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = categoria.getCategoria();
                if (findCategoria(id) == null) {
                    throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria categoria;
            try {
                categoria = em.getReference(Categoria.class, id);
                categoria.getCategoria();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Platillo> platilloCollectionOrphanCheck = categoria.getPlatilloCollection();
            for (Platillo platilloCollectionOrphanCheckPlatillo : platilloCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Categoria (" + categoria + ") cannot be destroyed since the Platillo " + platilloCollectionOrphanCheckPlatillo + " in its platilloCollection field has a non-nullable idCategoria field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Restaurant> restaurantCollection = categoria.getRestaurantCollection();
            for (Restaurant restaurantCollectionRestaurant : restaurantCollection) {
                restaurantCollectionRestaurant.getCategoriaCollection().remove(categoria);
                restaurantCollectionRestaurant = em.merge(restaurantCollectionRestaurant);
            }
            em.remove(categoria);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categoria> findCategoriaEntities() {
        return findCategoriaEntities(true, -1, -1);
    }

    public List<Categoria> findCategoriaEntities(int maxResults, int firstResult) {
        return findCategoriaEntities(false, maxResults, firstResult);
    }

    private List<Categoria> findCategoriaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categoria.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categoria findCategoria(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categoria.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categoria> rt = cq.from(Categoria.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
