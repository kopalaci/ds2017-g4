/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.DBManager;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import espol.edu.ec.model.Categoria;
import java.util.ArrayList;
import java.util.Collection;
import espol.edu.ec.model.Usuario;
import espol.edu.ec.model.Platillo;
import espol.edu.ec.model.Restaurant;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Palacios
 */
public class RestaurantJpaController implements Serializable {

    public RestaurantJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public RestaurantJpaController() {
    }

    public EntityManager getEntityManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("CatalogoDeDelicias2PU");
        }
        return emf.createEntityManager();
    }

    public void create(Restaurant restaurant) {
        if (restaurant.getCategoriaCollection() == null) {
            restaurant.setCategoriaCollection(new ArrayList<Categoria>());
        }
        if (restaurant.getUsuarioCollection() == null) {
            restaurant.setUsuarioCollection(new ArrayList<Usuario>());
        }
        if (restaurant.getPlatilloCollection() == null) {
            restaurant.setPlatilloCollection(new ArrayList<Platillo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Categoria> attachedCategoriaCollection = new ArrayList<Categoria>();
            for (Categoria categoriaCollectionCategoriaToAttach : restaurant.getCategoriaCollection()) {
                categoriaCollectionCategoriaToAttach = em.getReference(categoriaCollectionCategoriaToAttach.getClass(), categoriaCollectionCategoriaToAttach.getCategoria());
                attachedCategoriaCollection.add(categoriaCollectionCategoriaToAttach);
            }
            restaurant.setCategoriaCollection(attachedCategoriaCollection);
            Collection<Usuario> attachedUsuarioCollection = new ArrayList<Usuario>();
            for (Usuario usuarioCollectionUsuarioToAttach : restaurant.getUsuarioCollection()) {
                usuarioCollectionUsuarioToAttach = em.getReference(usuarioCollectionUsuarioToAttach.getClass(), usuarioCollectionUsuarioToAttach.getUsuario());
                attachedUsuarioCollection.add(usuarioCollectionUsuarioToAttach);
            }
            restaurant.setUsuarioCollection(attachedUsuarioCollection);
            Collection<Platillo> attachedPlatilloCollection = new ArrayList<Platillo>();
            for (Platillo platilloCollectionPlatilloToAttach : restaurant.getPlatilloCollection()) {
                platilloCollectionPlatilloToAttach = em.getReference(platilloCollectionPlatilloToAttach.getClass(), platilloCollectionPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollection.add(platilloCollectionPlatilloToAttach);
            }
            restaurant.setPlatilloCollection(attachedPlatilloCollection);
            em.persist(restaurant);
            for (Categoria categoriaCollectionCategoria : restaurant.getCategoriaCollection()) {
                categoriaCollectionCategoria.getRestaurantCollection().add(restaurant);
                categoriaCollectionCategoria = em.merge(categoriaCollectionCategoria);
            }
            for (Usuario usuarioCollectionUsuario : restaurant.getUsuarioCollection()) {
                Restaurant oldIdRestaurantOfUsuarioCollectionUsuario = usuarioCollectionUsuario.getIdRestaurant();
                usuarioCollectionUsuario.setIdRestaurant(restaurant);
                usuarioCollectionUsuario = em.merge(usuarioCollectionUsuario);
                if (oldIdRestaurantOfUsuarioCollectionUsuario != null) {
                    oldIdRestaurantOfUsuarioCollectionUsuario.getUsuarioCollection().remove(usuarioCollectionUsuario);
                    oldIdRestaurantOfUsuarioCollectionUsuario = em.merge(oldIdRestaurantOfUsuarioCollectionUsuario);
                }
            }
            for (Platillo platilloCollectionPlatillo : restaurant.getPlatilloCollection()) {
                Restaurant oldIdRestaurantOfPlatilloCollectionPlatillo = platilloCollectionPlatillo.getIdRestaurant();
                platilloCollectionPlatillo.setIdRestaurant(restaurant);
                platilloCollectionPlatillo = em.merge(platilloCollectionPlatillo);
                if (oldIdRestaurantOfPlatilloCollectionPlatillo != null) {
                    oldIdRestaurantOfPlatilloCollectionPlatillo.getPlatilloCollection().remove(platilloCollectionPlatillo);
                    oldIdRestaurantOfPlatilloCollectionPlatillo = em.merge(oldIdRestaurantOfPlatilloCollectionPlatillo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Restaurant restaurant) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Restaurant persistentRestaurant = em.find(Restaurant.class, restaurant.getIdRestaurant());
            Collection<Categoria> categoriaCollectionOld = persistentRestaurant.getCategoriaCollection();
            Collection<Categoria> categoriaCollectionNew = restaurant.getCategoriaCollection();
            Collection<Usuario> usuarioCollectionOld = persistentRestaurant.getUsuarioCollection();
            Collection<Usuario> usuarioCollectionNew = restaurant.getUsuarioCollection();
            Collection<Platillo> platilloCollectionOld = persistentRestaurant.getPlatilloCollection();
            Collection<Platillo> platilloCollectionNew = restaurant.getPlatilloCollection();
            List<String> illegalOrphanMessages = null;
            for (Platillo platilloCollectionOldPlatillo : platilloCollectionOld) {
                if (!platilloCollectionNew.contains(platilloCollectionOldPlatillo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Platillo " + platilloCollectionOldPlatillo + " since its idRestaurant field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Categoria> attachedCategoriaCollectionNew = new ArrayList<Categoria>();
            for (Categoria categoriaCollectionNewCategoriaToAttach : categoriaCollectionNew) {
                categoriaCollectionNewCategoriaToAttach = em.getReference(categoriaCollectionNewCategoriaToAttach.getClass(), categoriaCollectionNewCategoriaToAttach.getCategoria());
                attachedCategoriaCollectionNew.add(categoriaCollectionNewCategoriaToAttach);
            }
            categoriaCollectionNew = attachedCategoriaCollectionNew;
            restaurant.setCategoriaCollection(categoriaCollectionNew);
            Collection<Usuario> attachedUsuarioCollectionNew = new ArrayList<Usuario>();
            for (Usuario usuarioCollectionNewUsuarioToAttach : usuarioCollectionNew) {
                usuarioCollectionNewUsuarioToAttach = em.getReference(usuarioCollectionNewUsuarioToAttach.getClass(), usuarioCollectionNewUsuarioToAttach.getUsuario());
                attachedUsuarioCollectionNew.add(usuarioCollectionNewUsuarioToAttach);
            }
            usuarioCollectionNew = attachedUsuarioCollectionNew;
            restaurant.setUsuarioCollection(usuarioCollectionNew);
            Collection<Platillo> attachedPlatilloCollectionNew = new ArrayList<Platillo>();
            for (Platillo platilloCollectionNewPlatilloToAttach : platilloCollectionNew) {
                platilloCollectionNewPlatilloToAttach = em.getReference(platilloCollectionNewPlatilloToAttach.getClass(), platilloCollectionNewPlatilloToAttach.getIdPlatillo());
                attachedPlatilloCollectionNew.add(platilloCollectionNewPlatilloToAttach);
            }
            platilloCollectionNew = attachedPlatilloCollectionNew;
            restaurant.setPlatilloCollection(platilloCollectionNew);
            restaurant = em.merge(restaurant);
            for (Categoria categoriaCollectionOldCategoria : categoriaCollectionOld) {
                if (!categoriaCollectionNew.contains(categoriaCollectionOldCategoria)) {
                    categoriaCollectionOldCategoria.getRestaurantCollection().remove(restaurant);
                    categoriaCollectionOldCategoria = em.merge(categoriaCollectionOldCategoria);
                }
            }
            for (Categoria categoriaCollectionNewCategoria : categoriaCollectionNew) {
                if (!categoriaCollectionOld.contains(categoriaCollectionNewCategoria)) {
                    categoriaCollectionNewCategoria.getRestaurantCollection().add(restaurant);
                    categoriaCollectionNewCategoria = em.merge(categoriaCollectionNewCategoria);
                }
            }
            for (Usuario usuarioCollectionOldUsuario : usuarioCollectionOld) {
                if (!usuarioCollectionNew.contains(usuarioCollectionOldUsuario)) {
                    usuarioCollectionOldUsuario.setIdRestaurant(null);
                    usuarioCollectionOldUsuario = em.merge(usuarioCollectionOldUsuario);
                }
            }
            for (Usuario usuarioCollectionNewUsuario : usuarioCollectionNew) {
                if (!usuarioCollectionOld.contains(usuarioCollectionNewUsuario)) {
                    Restaurant oldIdRestaurantOfUsuarioCollectionNewUsuario = usuarioCollectionNewUsuario.getIdRestaurant();
                    usuarioCollectionNewUsuario.setIdRestaurant(restaurant);
                    usuarioCollectionNewUsuario = em.merge(usuarioCollectionNewUsuario);
                    if (oldIdRestaurantOfUsuarioCollectionNewUsuario != null && !oldIdRestaurantOfUsuarioCollectionNewUsuario.equals(restaurant)) {
                        oldIdRestaurantOfUsuarioCollectionNewUsuario.getUsuarioCollection().remove(usuarioCollectionNewUsuario);
                        oldIdRestaurantOfUsuarioCollectionNewUsuario = em.merge(oldIdRestaurantOfUsuarioCollectionNewUsuario);
                    }
                }
            }
            for (Platillo platilloCollectionNewPlatillo : platilloCollectionNew) {
                if (!platilloCollectionOld.contains(platilloCollectionNewPlatillo)) {
                    Restaurant oldIdRestaurantOfPlatilloCollectionNewPlatillo = platilloCollectionNewPlatillo.getIdRestaurant();
                    platilloCollectionNewPlatillo.setIdRestaurant(restaurant);
                    platilloCollectionNewPlatillo = em.merge(platilloCollectionNewPlatillo);
                    if (oldIdRestaurantOfPlatilloCollectionNewPlatillo != null && !oldIdRestaurantOfPlatilloCollectionNewPlatillo.equals(restaurant)) {
                        oldIdRestaurantOfPlatilloCollectionNewPlatillo.getPlatilloCollection().remove(platilloCollectionNewPlatillo);
                        oldIdRestaurantOfPlatilloCollectionNewPlatillo = em.merge(oldIdRestaurantOfPlatilloCollectionNewPlatillo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = restaurant.getIdRestaurant();
                if (findRestaurant(id) == null) {
                    throw new NonexistentEntityException("The restaurant with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Restaurant restaurant;
            try {
                restaurant = em.getReference(Restaurant.class, id);
                restaurant.getIdRestaurant();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The restaurant with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Platillo> platilloCollectionOrphanCheck = restaurant.getPlatilloCollection();
            for (Platillo platilloCollectionOrphanCheckPlatillo : platilloCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Restaurant (" + restaurant + ") cannot be destroyed since the Platillo " + platilloCollectionOrphanCheckPlatillo + " in its platilloCollection field has a non-nullable idRestaurant field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Categoria> categoriaCollection = restaurant.getCategoriaCollection();
            for (Categoria categoriaCollectionCategoria : categoriaCollection) {
                categoriaCollectionCategoria.getRestaurantCollection().remove(restaurant);
                categoriaCollectionCategoria = em.merge(categoriaCollectionCategoria);
            }
            Collection<Usuario> usuarioCollection = restaurant.getUsuarioCollection();
            for (Usuario usuarioCollectionUsuario : usuarioCollection) {
                usuarioCollectionUsuario.setIdRestaurant(null);
                usuarioCollectionUsuario = em.merge(usuarioCollectionUsuario);
            }
            em.remove(restaurant);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Restaurant> findRestaurantEntities() {
        return findRestaurantEntities(true, -1, -1);
    }

    public List<Restaurant> findRestaurantEntities(int maxResults, int firstResult) {
        return findRestaurantEntities(false, maxResults, firstResult);
    }

    private List<Restaurant> findRestaurantEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Restaurant.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Restaurant findRestaurant(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Restaurant.class, id);
        } finally {
            em.close();
        }
    }

    public int getRestaurantCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Restaurant> rt = cq.from(Restaurant.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
