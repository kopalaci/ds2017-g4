/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.DBManager;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import espol.edu.ec.model.Categoria;
import espol.edu.ec.model.Restaurant;
import espol.edu.ec.model.Ingrediente;
import java.util.ArrayList;
import java.util.Collection;
import espol.edu.ec.model.Imagen;
import espol.edu.ec.model.Platillo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Palacios
 */
public class PlatilloJpaController implements Serializable {

    public PlatilloJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("CatalogoDeDelicias2PU");
        }
        return emf.createEntityManager();
    }

    public PlatilloJpaController() {
    }
    

    public void create(Platillo platillo) {
        if (platillo.getIngredienteCollection() == null) {
            platillo.setIngredienteCollection(new ArrayList<Ingrediente>());
        }
        if (platillo.getImagenCollection() == null) {
            platillo.setImagenCollection(new ArrayList<Imagen>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria idCategoria = platillo.getIdCategoria();
            if (idCategoria != null) {
                idCategoria = em.getReference(idCategoria.getClass(), idCategoria.getCategoria());
                platillo.setIdCategoria(idCategoria);
            }
            Restaurant idRestaurant = platillo.getIdRestaurant();
            if (idRestaurant != null) {
                idRestaurant = em.getReference(idRestaurant.getClass(), idRestaurant.getIdRestaurant());
                platillo.setIdRestaurant(idRestaurant);
            }
            Collection<Ingrediente> attachedIngredienteCollection = new ArrayList<Ingrediente>();
            for (Ingrediente ingredienteCollectionIngredienteToAttach : platillo.getIngredienteCollection()) {
                ingredienteCollectionIngredienteToAttach = em.getReference(ingredienteCollectionIngredienteToAttach.getClass(), ingredienteCollectionIngredienteToAttach.getIdIngrediente());
                attachedIngredienteCollection.add(ingredienteCollectionIngredienteToAttach);
            }
            platillo.setIngredienteCollection(attachedIngredienteCollection);
            Collection<Imagen> attachedImagenCollection = new ArrayList<Imagen>();
            for (Imagen imagenCollectionImagenToAttach : platillo.getImagenCollection()) {
                imagenCollectionImagenToAttach = em.getReference(imagenCollectionImagenToAttach.getClass(), imagenCollectionImagenToAttach.getIdImagen());
                attachedImagenCollection.add(imagenCollectionImagenToAttach);
            }
            platillo.setImagenCollection(attachedImagenCollection);
            em.persist(platillo);
            if (idCategoria != null) {
                idCategoria.getPlatilloCollection().add(platillo);
                idCategoria = em.merge(idCategoria);
            }
            if (idRestaurant != null) {
                idRestaurant.getPlatilloCollection().add(platillo);
                idRestaurant = em.merge(idRestaurant);
            }
            for (Ingrediente ingredienteCollectionIngrediente : platillo.getIngredienteCollection()) {
                ingredienteCollectionIngrediente.getPlatilloCollection().add(platillo);
                ingredienteCollectionIngrediente = em.merge(ingredienteCollectionIngrediente);
            }
            for (Imagen imagenCollectionImagen : platillo.getImagenCollection()) {
                Platillo oldIdPatilloOfImagenCollectionImagen = imagenCollectionImagen.getIdPatillo();
                imagenCollectionImagen.setIdPatillo(platillo);
                imagenCollectionImagen = em.merge(imagenCollectionImagen);
                if (oldIdPatilloOfImagenCollectionImagen != null) {
                    oldIdPatilloOfImagenCollectionImagen.getImagenCollection().remove(imagenCollectionImagen);
                    oldIdPatilloOfImagenCollectionImagen = em.merge(oldIdPatilloOfImagenCollectionImagen);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Platillo platillo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Platillo persistentPlatillo = em.find(Platillo.class, platillo.getIdPlatillo());
            Categoria idCategoriaOld = persistentPlatillo.getIdCategoria();
            Categoria idCategoriaNew = platillo.getIdCategoria();
            Restaurant idRestaurantOld = persistentPlatillo.getIdRestaurant();
            Restaurant idRestaurantNew = platillo.getIdRestaurant();
            Collection<Ingrediente> ingredienteCollectionOld = persistentPlatillo.getIngredienteCollection();
            Collection<Ingrediente> ingredienteCollectionNew = platillo.getIngredienteCollection();
            Collection<Imagen> imagenCollectionOld = persistentPlatillo.getImagenCollection();
            Collection<Imagen> imagenCollectionNew = platillo.getImagenCollection();
            if (idCategoriaNew != null) {
                idCategoriaNew = em.getReference(idCategoriaNew.getClass(), idCategoriaNew.getCategoria());
                platillo.setIdCategoria(idCategoriaNew);
            }
            if (idRestaurantNew != null) {
                idRestaurantNew = em.getReference(idRestaurantNew.getClass(), idRestaurantNew.getIdRestaurant());
                platillo.setIdRestaurant(idRestaurantNew);
            }
            Collection<Ingrediente> attachedIngredienteCollectionNew = new ArrayList<Ingrediente>();
            for (Ingrediente ingredienteCollectionNewIngredienteToAttach : ingredienteCollectionNew) {
                ingredienteCollectionNewIngredienteToAttach = em.getReference(ingredienteCollectionNewIngredienteToAttach.getClass(), ingredienteCollectionNewIngredienteToAttach.getIdIngrediente());
                attachedIngredienteCollectionNew.add(ingredienteCollectionNewIngredienteToAttach);
            }
            ingredienteCollectionNew = attachedIngredienteCollectionNew;
            platillo.setIngredienteCollection(ingredienteCollectionNew);
            Collection<Imagen> attachedImagenCollectionNew = new ArrayList<Imagen>();
            for (Imagen imagenCollectionNewImagenToAttach : imagenCollectionNew) {
                imagenCollectionNewImagenToAttach = em.getReference(imagenCollectionNewImagenToAttach.getClass(), imagenCollectionNewImagenToAttach.getIdImagen());
                attachedImagenCollectionNew.add(imagenCollectionNewImagenToAttach);
            }
            imagenCollectionNew = attachedImagenCollectionNew;
            platillo.setImagenCollection(imagenCollectionNew);
            platillo = em.merge(platillo);
            if (idCategoriaOld != null && !idCategoriaOld.equals(idCategoriaNew)) {
                idCategoriaOld.getPlatilloCollection().remove(platillo);
                idCategoriaOld = em.merge(idCategoriaOld);
            }
            if (idCategoriaNew != null && !idCategoriaNew.equals(idCategoriaOld)) {
                idCategoriaNew.getPlatilloCollection().add(platillo);
                idCategoriaNew = em.merge(idCategoriaNew);
            }
            if (idRestaurantOld != null && !idRestaurantOld.equals(idRestaurantNew)) {
                idRestaurantOld.getPlatilloCollection().remove(platillo);
                idRestaurantOld = em.merge(idRestaurantOld);
            }
            if (idRestaurantNew != null && !idRestaurantNew.equals(idRestaurantOld)) {
                idRestaurantNew.getPlatilloCollection().add(platillo);
                idRestaurantNew = em.merge(idRestaurantNew);
            }
            for (Ingrediente ingredienteCollectionOldIngrediente : ingredienteCollectionOld) {
                if (!ingredienteCollectionNew.contains(ingredienteCollectionOldIngrediente)) {
                    ingredienteCollectionOldIngrediente.getPlatilloCollection().remove(platillo);
                    ingredienteCollectionOldIngrediente = em.merge(ingredienteCollectionOldIngrediente);
                }
            }
            for (Ingrediente ingredienteCollectionNewIngrediente : ingredienteCollectionNew) {
                if (!ingredienteCollectionOld.contains(ingredienteCollectionNewIngrediente)) {
                    ingredienteCollectionNewIngrediente.getPlatilloCollection().add(platillo);
                    ingredienteCollectionNewIngrediente = em.merge(ingredienteCollectionNewIngrediente);
                }
            }
            for (Imagen imagenCollectionOldImagen : imagenCollectionOld) {
                if (!imagenCollectionNew.contains(imagenCollectionOldImagen)) {
                    imagenCollectionOldImagen.setIdPatillo(null);
                    imagenCollectionOldImagen = em.merge(imagenCollectionOldImagen);
                }
            }
            for (Imagen imagenCollectionNewImagen : imagenCollectionNew) {
                if (!imagenCollectionOld.contains(imagenCollectionNewImagen)) {
                    Platillo oldIdPatilloOfImagenCollectionNewImagen = imagenCollectionNewImagen.getIdPatillo();
                    imagenCollectionNewImagen.setIdPatillo(platillo);
                    imagenCollectionNewImagen = em.merge(imagenCollectionNewImagen);
                    if (oldIdPatilloOfImagenCollectionNewImagen != null && !oldIdPatilloOfImagenCollectionNewImagen.equals(platillo)) {
                        oldIdPatilloOfImagenCollectionNewImagen.getImagenCollection().remove(imagenCollectionNewImagen);
                        oldIdPatilloOfImagenCollectionNewImagen = em.merge(oldIdPatilloOfImagenCollectionNewImagen);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = platillo.getIdPlatillo();
                if (findPlatillo(id) == null) {
                    throw new NonexistentEntityException("The platillo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Platillo platillo;
            try {
                platillo = em.getReference(Platillo.class, id);
                platillo.getIdPlatillo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The platillo with id " + id + " no longer exists.", enfe);
            }
            Categoria idCategoria = platillo.getIdCategoria();
            if (idCategoria != null) {
                idCategoria.getPlatilloCollection().remove(platillo);
                idCategoria = em.merge(idCategoria);
            }
            Restaurant idRestaurant = platillo.getIdRestaurant();
            if (idRestaurant != null) {
                idRestaurant.getPlatilloCollection().remove(platillo);
                idRestaurant = em.merge(idRestaurant);
            }
            Collection<Ingrediente> ingredienteCollection = platillo.getIngredienteCollection();
            for (Ingrediente ingredienteCollectionIngrediente : ingredienteCollection) {
                ingredienteCollectionIngrediente.getPlatilloCollection().remove(platillo);
                ingredienteCollectionIngrediente = em.merge(ingredienteCollectionIngrediente);
            }
            Collection<Imagen> imagenCollection = platillo.getImagenCollection();
            for (Imagen imagenCollectionImagen : imagenCollection) {
                imagenCollectionImagen.setIdPatillo(null);
                imagenCollectionImagen = em.merge(imagenCollectionImagen);
            }
            em.remove(platillo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Platillo> findPlatilloEntities() {
        return findPlatilloEntities(true, -1, -1);
    }

    public List<Platillo> findPlatilloEntities(int maxResults, int firstResult) {
        return findPlatilloEntities(false, maxResults, firstResult);
    }

    private List<Platillo> findPlatilloEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Platillo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Platillo findPlatillo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Platillo.class, id);
        } finally {
            em.close();
        }
    }

    public int getPlatilloCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Platillo> rt = cq.from(Platillo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
