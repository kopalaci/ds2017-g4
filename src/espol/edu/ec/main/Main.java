/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.DBManager.UsuarioJpaController;
import espol.edu.ec.controller.AdminProfileController;
import espol.edu.ec.controller.AssistantProfileController;
import espol.edu.ec.controller.LoginController;
import espol.edu.ec.controller.ProfileController;

import espol.edu.ec.controller.UserProfileController;
import espol.edu.ec.model.Usuario;
import espol.edu.ec.security.Authenticator;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Palacios
 */
public class Main extends Application {
    
    private Stage stage;
    private Usuario loggedUser;
    private final double MINIMUM_WINDOW_WIDTH = 800;
    private final double MINIMUM_WINDOW_HEIGHT = 500.0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[])null);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setResizable(false);
        try {
            stage = primaryStage;
            stage.setTitle("Catalogo de Delicias");
            stage.setMinWidth(MINIMUM_WINDOW_WIDTH);
            stage.setMinHeight(MINIMUM_WINDOW_HEIGHT);
            gotoLogin();
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Usuario getLoggedUser() {
        return loggedUser;
    }
    
    
    private void gotoProfile() {
        try {
            if("cliente".equalsIgnoreCase(loggedUser.getTipo())){
                UserProfileController controller = new UserProfileController(loggedUser);
                loadProfile(controller);
            }
            if("asistente".equalsIgnoreCase(loggedUser.getTipo())){
                AssistantProfileController controller = new AssistantProfileController(loggedUser);
                loadProfile(controller);
            }
            if("administrador".equalsIgnoreCase(loggedUser.getTipo())){
                AdminProfileController controller = new AdminProfileController(loggedUser);
                loadProfile(controller);
            }

        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int userLogging(String userId, String password){
        int status = Authenticator.validate(userId, password);
        if (status ==1) {
            UsuarioJpaController userController = new UsuarioJpaController();
            loggedUser = userController.findUsuario(userId);
            gotoProfile();
            return 1;
        }
        return status;
    }
    
    public void userLogout(){
        loggedUser = null;
        gotoLogin();
    }
    
    

    private void gotoLogin() {
        try {
            LoginController login = (LoginController) replaceSceneContent("/espol/edu/ec/view/login.fxml");
            login.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));
        AnchorPane page;
        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        } 
        Scene scene = new Scene(page, 800, 500);
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }
        
    public void loadProfile(ProfileController controller){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/espol/edu/ec/view/profile.fxml"));
        loader.setController(controller);
        AnchorPane usuario= null;
        try {
            usuario = loader.load();
        } catch (IOException ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller.setApp(this);
        Scene scene = new Scene(usuario, 800, 500);
        stage.setScene(scene);
               
    }
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    
    

    
}
